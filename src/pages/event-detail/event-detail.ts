import { BuyTicketPage } from './../buy-ticket/buy-ticket';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the EventDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-event-detail',
  templateUrl: 'event-detail.html',
})
export class EventDetailPage {

  evento: any;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.evento = navParams.get('selected');
    console.log(this.evento);
  }

  ionViewDidLoad() {
  }

  buyTickets(eventSelected: any){
    this.navCtrl.push(BuyTicketPage, { 'selected': this.evento });
  }

}
