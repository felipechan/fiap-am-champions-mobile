import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { EventProvider } from '../../providers/event/event';
import { EventDetailPage } from '../event-detail/event-detail';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  eventsList: any;

  constructor(
    public navCtrl: NavController,
    private eventProvider: EventProvider
  ) {
    this.getEvents();
  }

  getEvents(){
    this.eventsList = this.eventProvider.getEvents();
  }

  getDetails(eventSelected: any){
    this.navCtrl.push(EventDetailPage, {'selected': eventSelected});
  }

}
