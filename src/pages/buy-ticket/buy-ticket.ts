import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';

/**
 * Generated class for the BuyTicketPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-buy-ticket',
  templateUrl: 'buy-ticket.html',
})
export class BuyTicketPage {

  evento: any;
  formFields: any = {
    nome: '',
    sobrenome: '',
    idade: 0,
    telefone: 0,
    cidade: ''
  }

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    private alertCtrl: AlertController
  ) {
    this.evento = navParams.get('selected');
    console.log(this.evento);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad BuyTicketPage');
  }

  validaCampos(){
    return true;
  }

  finish(){
    if(!this.validaCampos())
      return;
    let showAlert = this.alertCtrl.create({
      title:'Sucesso',
      subTitle: 'Sua compra foi finalizada com sucesso.',
      buttons: ['Ok']
    });
    showAlert.present();
    this.navCtrl.goToRoot(null);
  }

}
