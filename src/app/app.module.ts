import { BuyTicketPage } from './../pages/buy-ticket/buy-ticket';
import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { EventProvider } from '../providers/event/event';
import { EventDetailPage } from '../pages/event-detail/event-detail';
import { LoginPage } from '../pages/login/login';
import { HttpClientProvider } from '../providers/http-client/http-client';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    EventDetailPage,
    BuyTicketPage,
    LoginPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    EventDetailPage,
    BuyTicketPage,
    LoginPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    EventProvider,
    HttpClientProvider
  ]
})
export class AppModule {}
