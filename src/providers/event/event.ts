// import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';


@Injectable()
export class EventProvider {

  constructor() {
    
  }

  getEvents(){
    return [
      {
        'nome': 'Campeonato de LOL da FIAP',
        'dataEvento': '05/12/2018',
        'descricao': 'Esse é um evento interno para alunos da FIAP jogadores de CS GO',
        'local': 'Avenida Lins de Vasconcelos',
        'preco': 20
      },
      {
        'nome': 'Campeonato de Futebol',
        'dataEvento': '10/12/2018',
        'descricao': 'Esse é um evento aberto para jogadores de Futebol',
        'local': 'Anhembi',
        'preco': 40
      },
      {
        'nome': 'Campeonato de CS Players',
        'dataEvento': '15/12/2018',
        'descricao': 'Esse é um evento interno para jogadores de CS GO',
        'local': 'online',
        'preco': 50
      },
      {
        'nome': 'Torneio universitário de tênis de mesa',
        'dataEvento': '20/12/2018',
        'descricao': 'Esse é um evento para para jogadores de tênis de mesa',
        'local': 'Mackenzie',
        'preco': 30
      }
    ]
  }

  getEventDetail(){

  }

}
