import { AlertController } from 'ionic-angular';
import { Environment } from '../environment';
import { Injectable } from '@angular/core';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/Rx';

/*
  Esta classe gerencia as requisições CRUD
*/

@Injectable()
export class HttpClientProvider {

  apiUrl: string = 'https://login.salesforce.com/services/';
  private usuario: any;

  constructor(
    private http: Http,
    private alertCtrl: AlertController
  ) {
  }

  async setOptions(): Promise<any> {
    return new Promise(async (resolve, reject) => {
      await this.getToken().then((tkn) => {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        headers.append('Authorization', 'Bearer ' + tkn);
        let options = new RequestOptions({ headers: headers });
        resolve(options);
      }).catch((err) => reject(err))
    });
  }

  setOptionsPublic(headersParams?: any) {
    let options = new RequestOptions();
    options.headers = new Headers();
    options.headers.append('Content-Type', 'application/json');
    // this.options.headers.append('client_id', Environment.client_id);
  }

  async getToken(): Promise<any> {
    let tokenUrl =
      `${this.apiUrl}oauth2/token?grant_type=password&username=${Environment.username}&password=${Environment.password}${Environment.securityToken}&client_id=${Environment.client_id}&client_secret=${Environment.client_secret}`;

    debugger;
    return this.http.post(tokenUrl, {}).map(response => {
      return response.json() || { success: false, message: 'No response from server.' };
    }).catch((error: Response | any) => {
      return Observable.throw(error.json());
    }).toPromise();
  }

  async geraToken() {
    return new Promise(async (resolve, reject) => {
      await this.setOptionsPublic();
      try {
        // let tkn = await this.get(Environment.apiUrl + 'public/token', this.options);
        // this.storage.set('token', tkn).then(() => resolve(tkn.token_code));
      } catch (error) {
        reject(error);
      }
    });
  }

  async get(url: string, option?: any): Promise<any> {
    let optionsRequest
    if (option)
      optionsRequest = option;
    else
      optionsRequest = await this.setOptions();

    return this.http.get(url, optionsRequest).map(response => {
      return response.json() || { success: false, message: 'No response from server.' };
    }).catch((error: Response | any) => {
      this.exibirAlerta('Erro', 'Houve um problema. Verifique sua conexão com a internet ou contate um administrador.');
      return Observable.throw(error.json());
    }).toPromise();
  }

  async post(url: string, body: any, headersParams?: Headers): Promise<any> {
    let options = await this.setOptions(headersParams);
    return await this.http.post(url, body, this.options).map(response => {
      return response.json() || { success: false, message: 'No response from server.' };
    }).catch((error: Response | any) => {
      return Observable.throw(error.json());
    }).toPromise();
  }

  async put(url: string, body: any, option?: any): Promise<any> {

    if (option) {
      await this.setOptions(option);
    }

    return this.http.put(url, body, this.options).map(response => {
      return response.json() || { success: false, message: 'No response from server.' };
    }).catch((error: Response | any) => {
      return Observable.throw(error.json());
    }).toPromise();
  }

  delete(url: string, body: any, options?: any) {

  }

  exibirAlerta(titulo: string, mensagem: string) {
    const alert = this.alertCtrl.create({
      title: titulo,
      subTitle: mensagem,
      buttons: [
        {
          text: 'Ok',
          role: 'cancel',
          handler: () => {
            alert.dismiss();
            return false;
          }
        }
      ]
    });
    alert.present();
  }

}
