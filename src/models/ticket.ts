class Ticket{

    idTicket: number;
    idEvent: number;
    idCliente: number;
    dataCompra: Date;
    tipoPagamento: String;
    preco: number; //Double no banco

}


// 1 cliente pode ter 1 ingresso pra 1 evento
// 1 cliente pode ter ingressos diferentes para diferentes eventos
// 1 evento pode ter vários ingressos